# Introduction

This repository contains a simple flash driver for the SEGA Master System console, allowing to write data into flash-based cartridges, such as the [FrugalMapper](https://gitlab.com/doragasu/sms-sl2map).

This will allow you to save game progress, rankings, etc. without the need of a more expensive SRAM enabled cart. As long as the cartridge is flash based and implements SEGA Mapper slot 2 bankswitching, this code should work.

# Usage

The module usage is pretty straightforward: `flash.h` exports functions to read flash chip IDs, erase sectors, program a single byte and program an array of bytes.

You can see a simple usage example using devkitSMS [here](https://gitlab.com/doragasu/frugal-map-save-example).

# Some warnings

My Z80 skills are newbie level, so the included assembly is cumbersome (merge requests are welcome!) and you should consider using it risky! Test your save functions thoroughly to make sure the code has no bugs that can corrupt your save data or even worse erase your game code!

Also, as you are writing on a flash device, you have to be careful with the usual stuff when flash devices are involved:

* Data in the chip is organized in sectors. You can write data in byte granularity, but erase operations have sector granularity (i.e. it is not possible to erase a single byte, a complete sector must be erased). Sector size is chip dependant. The SST39SF040 found on FrugalMap carts has 4 KiB long sectors.
* Erased data has the value 0xFF.
* In flash chips, writing data to the chip is called "programming". When programming data, you must make sure the target location is erased (has value 0xFF). If you try writing data to a non erased location the machine might lock.
* Code dealing with the flash chip disables interrupts and re-enables them when finished. This typically interferes with sound players. So you should make sure the PSG is stopped when erasing/programming data.
* Be careful to not accidentally erase your game code!
* Flash chips suffer from wear, so you have to be careful to not do more program/erase operations than needed. But do not panic, typically chips are guaranteed for at least 100.000 program/erase cycles, so it is really difficult to damage a chip if you just save data when the user performs a progress save or enters a high-score.
* If power is cut while one program/erase operation is in progress, you will most likely loose data unless you implement your own safe save mechanism. As a suggestion, you can do it by using two sectors for saving: active sector and inactive sector. You save to the inactive sector, and when finished mark it as active and erase the previous active one. If power is cut before the inactive is marked as active, you still have the data in the original active sector. If power is cut when you mark the inactive as active and prior to erasing the previously active sector, the next power up you will have two active sectors, any of them will do. As an example, you can see a safe save implementation for the Megadrive console (also supporting smart saving to reduce flash wear and variable length slot support) [here](https://gitlab.com/doragasu/sgdk-flash-save).

Finally, you should keep in mind that at the time of writing this file, no SMS emulators support flash erase/write emulation, so this has to be taken into account during development. You could maybe support SRAM save during development, and switch to flash save for the final ROM.

# Author

The code in this repository has been written by Jesús Alonso (doragasu). Merge requests are welcome!

# License

This code is provided with NO WARRANTY under the MIT license.
